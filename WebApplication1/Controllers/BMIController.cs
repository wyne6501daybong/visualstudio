﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (data.Height != null && data.Weight != null)
            {
                var m_height = data.Height / 100;
                var result = data.Weight / (m_height * m_height);
                var level="";

                data.Result = (float)result;
                data.BMI = result;
                data.Level = level;
                if (result < 18.5)
                    level = "體重過輕";
                else if (result >= 18.5 && result < 24)
                    level = "正常範圍";
                else if (result >= 24 && result < 27)
                    level = "過重";
                else if (result >= 27 && result < 30)
                    level = "輕度肥胖";
                else if (result >= 30 && result < 35)
                    level = "中度肥胖";
                else
                    level = "重度肥胖";

                


            }

            return View();
        }
        // GET: BMI
        /*public ActionResult Index(float? weight, float? height)
        {
            if (height != null && weight != null)
            {
                var m_height = height.Value / 100;
                var result = weight.Value / (m_height * m_height);

                ViewBag.Result = result;
                if(result<18.5)
                    ViewBag.Level = "體重過輕";
                else if(result>=18.5&&result<24)
                    ViewBag.Level = "正常範圍";
                else if(result>=24&&result<27)
                    ViewBag.Level = "過重";
                else if(result>=27&&result<30)
                    ViewBag.Level = "輕度肥胖";
                else if (result >= 30 && result < 35)
                    ViewBag.Level = "中度肥胖";
                else
                    ViewBag.Level = "重度肥胖";

                ViewBag.H = height;
                ViewBag.W = weight;


            }
            
            return View();
        }*/
    }
}