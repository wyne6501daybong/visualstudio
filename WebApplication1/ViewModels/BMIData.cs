﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModels
{
    public class BMIData
    {
        public float? Height { get; set; }
        public float? Weight { get; set; }
        public float Result { get; set; }
        public String Level { get; set; }
        public float? BMI { get; set; }
    }
}